### Project Title: Mitches' Database and Meal Planner
### Team Members:
+ Cameron Lindsey
+ Keenan Andrews


### Description
This project will implement a database containing every food that mitches currently serves. The database will be connected
to a web server using Javascript, CSS, and HTML. The web sever will allow a user to create a username and password, and
then proceed to view all upcoming meals, a list of all foods and their calories, carbs, fat, saturated fat, protein, fiber,
and Sodium. In addition it will allow a user to customize meal plans for the day, show the user what foods are good for
particular diets. In addition it will also allow a user to add any personal foods to the database, as well as create a
personal diet.
The webserver will give an easy way for cadets to access the nutritional information of Mitchell Hall meals as well as being able 
to plan meals around a personal diet.

### Installation Instructions
List here any specific installation instructions required to initialize the web application. If you followed the submission instructions it should be as simple as:

+ Requires MongoDB/MySQL Server to be running
+ `initialize.bat` to resolve dependencies and to seed the MongoDB/MySQL server
+ `npm start` to begin the application

### Functionality Shortfalls
List here any functionality shortfalls in your program. Identify those specific areas that your submission doesn't meet the assignment requirements or your own design specification. This section will be updated for both Sprint 1 and the Final Submission

### Specification
1. List your application requirements here
    1. You can list sub-bullets by indenting with four spaces
    2. Your requirements should be as specific as possible
1. The requirements should be written from the view of the 'user', not a 'developer'
    1. For example, 'uses JQuery' is not a requirement for this discussion
    2. Rather, 'User receives a visual warning when he enters an invalid date in the date field' is an appropriate requirement. It would be appropriate to have several such requirements for each input field
    3. Other sample requirements:
        1. Malicious data from user is not submitted to the database
        2. Malicious data from user is not executed as a route
        3. Malicious data from user is not stored or represented on the HTML page
        4. On the user information page, all user information is properly presented with visual appeal

### Development Plan
#### Sprint 1:
+ Team Member 1: Description of work for Team Member 1
+ Team Member 2: Description of work for Team Member 2
+ Team Member 3: Description of work for Team Member 3
+ Requirements for Sprint 1 (listed below are examples, but they should correspond to your own requirements enumeration):
    1. Requirement 2.7
    2. Requirement 3.8
#### Final:
+ Team Member 1: Description of work for Team Member 1
+ Team Member 2: Description of work for Team Member 2
+ Team Member 3: Description of work for Team Member 3

### Documentation:
#### Tracer Bullet Submission
 1. Tracer Bullet specification documentation statement 1
 2. Tracer Bullet specification documentation statement 2
 3. Tracer Bullet specification documentation statement 3

#### Design Specification Submission
 1. Design specification documentation statement 1
 2. Design specification documentation statement 2
 3. Design specification documentation statement 3

#### Sprint 1 Submission
 1. Sprint 1 documentation statement 1
 2. Sprint 1 documentation statement 1
 3. Spring 1 documentation statement 1

#### Final Submission
 1. Final Submission specification documentation statement 1
 2. Final Submission specification documentation statement 2
 3. Final Submission specification documentation statement 2
